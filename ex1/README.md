# Exercise 1 - Let's catch a ping

In this exercise we'll run ping agains a well known server on the internet and try to catch a few "ping" packets.

## Outline

### Step 1

Start a live capture on the network interface connecting you to the internet.

### Step 2

Open a terminal window and start "pinging" a well known address, for example:

```
ping 8.8.8.8
```

### Step 3

Enter ```icmp``` as the filter expression.

## Questions

  1. What are the two ICMP message types (name and number)? 
  2. Can you think of a different filter expression that you could use and would show you the same packets?
