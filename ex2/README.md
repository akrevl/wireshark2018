# Exercise 2 - Network topology

Let's try to figure out how we are connected to the Internet.

## Outline

### Step 1

Open a terminal window and run ```traceroute``` to different hosts on the Internet. You'll get a better idea of the topology if you try hosts in different countries. You could also try to check if there is a difference between the "commercial" Internet and the "academic" Internet.

### Step 2

Now that you've identified different routers add them to the network topology diagram on the whiteboard (try to figure out how your path fits in the bigger picture).

## Questions

  1. Which property does traceroute use to find routers on a path?
  2. Which ICMP messages are used?
  3. How many router hops does it take to get to Germany?
  4. What is the maximum number of hops that your operating system sets by default?
