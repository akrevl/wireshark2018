# Exercise 6 - IMs'r'us

All of the protocols in this exercise were used for instant messaging. 

## Outline

### Step 1

Download files ```ex6_a.cap```,  ```ex5_b.cap```, ```ex5_c.cap```  and open them with Wireshark. 

## Questions

  1. Which protocol is used in ```ex6_a.cap```? Which channels were joined?
  2. What website was using the chat protocol in ```ex6_b.cap```? Can you identify the message?
  3. Which IM protocol is used in ```ex6_c.cap```? Which emails are communicating?
  4. Which IM protocol is used in ```ex6_d.cap```? Can you spot the message sent?

