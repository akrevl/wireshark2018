# Exercise 7 - poor man's encryption

Sometimes things look like random jibberish. But they're not.

## Outline

### Step 1

Download the file ```ex7.cap``` and open it with Wireshark. 

## Questions

  1. What is the protocol used?
  2. What are the answers received from the server (status codes)? What do they mean?
  3. In the end some form of authentication is used. What kind? What are the username and password?

