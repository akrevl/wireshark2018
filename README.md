# Wireshark Workshop 2018

This is the repo for the Wireshark Workshop held for Applied Cyber at Stanford on Friday, April 27th, 2018.

## Cloning the repository

The easiest way to get all the files used in the workshop is to clone this repository. You can use the following command:

```
git clone https://bitbucket.org/akrevl/wireshark2018.git
```

Alternatively you can use your web browser to get the files directly from the repository. 

## Exercises

Most of the exercises include packet caputre files and a README file with the instructions and some possibly interesting questions.

  * [Exercise 1](ex1/README.md) - Let's catch a ping
  * [Exercise 2](ex2/README.md) - Network topology
  * [Exercise 3](ex3/README.md) - Following a stream
  * [Exercise 4](ex4/README.md) - Some UDP
  * [Exercise 5](ex5/README.md) - You've got mail
  * [Exercise 6](ex6/README.md) - IMs'r'us
  * [Exercise 7](ex7/README.md) - Poor man's encryption
  * [Exercise 8](ex8/README.md) - HTTP data
  * [Exercise 9](ex9/README.md) - Weird SSL

