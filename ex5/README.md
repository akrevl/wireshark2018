# Exercise 5 - you've got mail

All of the protocols in this exercise are connected with e-mail.

## Outline

### Step 1

Download files ```ex5_a.cap```,  ```ex5_b.cap```, ```ex5_c.cap```  and open them with Wireshark. 

## Questions

  1. Identify the different protocols in all the files you downloaded.
  2. Looking at ```ex5_a.cap``` do you think it would be hard to forge an email? Do you think this can be mitigated with authentication? What about TLS?
  3. In ```ex5_b.cap``` what are the username and password? What is the content type of the message?
  4. In ```ex5_c.cap``` what are the username and password? How many messages are there in INBOX?
  5. Can you see the username and password in ```ex5_d.cap```? Why not? Do you think that is good protection?

