# Exercise 3 - Following a stream

Let's look at some capture files and get comfortable using filters and other Wireshark features.

## Outline

### Step 1

Download ```ex3.cap``` and open it with Wireshark. 

### Step 2

Find packet #4 right click on it and choose "Follow TCP stream."

## Questions

  1. Which protocol are you looking at?
  2. Can you tell which browser was used to produce this packet capture?
  3. Can you tell which OS was used to produce the packets in this packet capture?
  4. What is the content about?
  5. What does the header ```Last-modified``` mean?
