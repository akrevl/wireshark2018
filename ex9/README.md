# Exercise 9 - weird SSL

## Outline

### Step 1

Download the file ```ex9.pcap``` and open it with Wireshark.

### Step 2

Follow the TCP stream of the TCP session with the destination port 1234.

### Step 3

Right click on any of the packets and choose "Decode As..." Choose SSL to decode the TCP stream as an SSL session.

## Questions

  1. Which cipher is used in the SSL session in the capture file?

