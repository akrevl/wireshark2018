# Exercise 8 - HTTP data

There are (at least) two ways of sending data to HTTP servers.

## Outline

### Step 1

Download the files ```http_get.cap```, ```http_post.cap``` and open it with Wireshark. 

## Questions

  1. What is the main difference between the data that is sent in the two capture files?

