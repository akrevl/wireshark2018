# Exercise 4 - Some UDP

Let's look at some capture files and get comfortable using filters and other Wireshark features.

## Outline

### Step 1

Download ```ex4.cap``` and open it with Wireshark. 

## Questions

  1. Which protocol are you looking at?
  2. What is the IP address of the server?
  3. What type of a record are we asking for?
  4. What is the answer to our query (IP)? 
  5. Do you know what the "Additional records" are used for?
  6. Now run a live capture and use ```nslookup``` to find the mail server for CS.Stanford.EDU.
